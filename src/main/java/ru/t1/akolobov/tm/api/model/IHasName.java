package ru.t1.akolobov.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
