package ru.t1.akolobov.tm.api.model;

import ru.t1.akolobov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
