package ru.t1.akolobov.tm.command.system;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    public static final String NAME = "version";

    public static final String ARGUMENT = "-v";

    public static final String DESCRIPTION = "Display application version.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.18.0");
    }

}
